package com.filez.bench.actor

import com.typesafe.config._
import akka.actor._
import org.apache.commons.lang3.time._

class Receiver extends Actor {

	val BATCH = 25000
	var counter:Int = 0
        var speedCounter:Int = 0
	var rttsum:Long = 0
        var speedsum:Long = 0
	val clock = new StopWatch()
    
	def receive = {
	    case t:Long =>
			val now = System.currentTimeMillis();
			val	rtt = now-t
			rttsum  += rtt
			counter += 1
			if (counter % BATCH == 0) {
                                System.out.println("Batch done in "+clock.getTime()+" ms")
                                if (clock.getTime() > 20 && clock.getTime() < 10000 ) {
                                	speedCounter += 1
                                	speedsum += (BATCH/(clock.getTime()/1000.0)).toLong
                                }
				System.out.println(counter+" rtt "+rtt+"ms, avg rtt "+(rttsum/counter)+" speed="+ BATCH/(clock.getTime()/1000.0))
                                if (speedCounter>0) System.out.println("\t\tavg="+ (speedsum/speedCounter))
				clock.reset();
				clock.start();
			}
	}
}