package com.filez.bench.actor

import com.typesafe.config._
import akka.actor._
import org.apache.commons.lang3.time._

object Sender {

    /** number of messages to send */
    val MSGS = 300000
    val BATCH = 50000
    /** message expiration time in milliseconds */
    val EXPIRE = 2*60*1000

    def batch(sender: (Any) => Unit) = {
       val clock = new StopWatch()
       val batch = new StopWatch()

       clock.start()
       batch.start()
       for(i <- 1 to MSGS) {
          sender(System.currentTimeMillis())
		  if(i % BATCH == 0) {
				System.out.println(i+" of "+MSGS+" sent, speed ="+(BATCH/(batch.getTime()/1000.0))+" msg/sec.");
				batch.reset();
				batch.start();
			}
       }
   	   clock.stop();
	   System.out.print(MSGS+" sent in "+DurationFormatUtils.formatDurationHMS(clock.getTime()));
	   System.out.println(", msg/sec="+MSGS/((clock.getTime())/1000.0));
    }
}