package com.filez.bench.actor

import com.typesafe.config._
import akka.actor._
import akka.util._
import akka.cluster._
import akka.cluster.pubsub._
//import akka.contrib.pattern._
import scala.concurrent.duration._

object Main extends App {

    sealed trait MODE
    object MODE {
      case object SENDER extends MODE
      case object RECEIVER extends MODE
    }
    val port:String = if (args.length > 0) args(0) else "0";
    val mode:MODE = if (args.length > 1 && args(1).toLowerCase() == "s") MODE.SENDER else MODE.RECEIVER 
    println(s"Starting cluster $mode on port $port")
	// override port from command line
	val config = ConfigFactory.parseString("akka.remote.artery.canonical.port=" + port).
		withFallback(ConfigFactory.load("application.conf"))

	// Create an Akka system
	val system = ActorSystem("ClusterSystem", config)
	implicit val context = system.dispatcher
	val mediator = DistributedPubSub(system).mediator
	system.scheduler.scheduleOnce(5.seconds) {
	mode match {
      case MODE.RECEIVER =>
        Cluster(system).registerOnMemberUp {
        	val rcv = system.actorOf(Props[Receiver].withDeploy(new Deploy(ClusterScope)), name="receiver")
        	mediator ! DistributedPubSubMediator.Put(rcv)
        	println(s"Receiver started on ref $rcv")
        }
      case MODE.SENDER =>
        Cluster(system).registerOnMemberUp{
        	println("cluster up, starting sender")
        	Sender.batch{ msg =>
        	  mediator ! DistributedPubSubMediator.Send("/user/receiver", msg, false)
        	}
        	system.scheduler.scheduleOnce(30.seconds) {
			Cluster(system).leave(Cluster(system).selfAddress)
			system.scheduler.scheduleOnce(2.seconds) {
        			system.terminate()
			}
        	}
        }
        /*
        val future = system.actorSelection("/user/receiver").resolveOne(30.seconds)
        future.onSuccess{ case rcv:ActorRef => println("receiver resolved, sending")
          Sender.send(rcv, system.deadLetters)
        }
        future.onFailure{ case t:Throwable =>
          println(s"lookup error $t")
          system.shutdown()}
        }
        */
    }
    }
}