Actor cluster benchmark
=======================

program takes 2 arguments, first is port number and second is mode 's' or 'r'
for sender or receiver.

To form cluster you need to run one receiver on port 2551, in sbt
type 'run 2551 r'

then start sender 'run 2552 s'

sender speed is irelevant because messages just got buffered

Results

|Scala|Akka|msg/sec|
|-----|----|-------|
|2.10 |2.3 |15328|
|2.11 |2.4 |13792|
|2.12 |2.4 |13526|
|2.12 |2.5 |16481|
|2.11 |2.5 |16400|
|2.13 |2.5 |16449|
|2.13 |2.6 |155777|
