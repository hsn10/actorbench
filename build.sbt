// scalaVersion := "2.11.12"

// scalaVersion := "2.12.11"

scalaVersion := "2.13.3"

scalacOptions ++= Seq("-deprecation", "-feature")

parallelExecution in Test := false

val akkaVersion = "2.6.6"

libraryDependencies ++= Seq(
    "org.apache.commons" % "commons-lang3" % "3.3.2",
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-cluster" % akkaVersion,
    "com.typesafe.akka" %% "akka-cluster-tools" % akkaVersion
)

libraryDependencies ++= Seq(
  "io.aeron" % "aeron-driver" % "1.27.0",
  "io.aeron" % "aeron-client" % "1.27.0"
)

fork := true
